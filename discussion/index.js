const express=require("express");
//mongoose is a package that allows creation of schemas to model the data structures and to have an access to a number of methods for manipulating the database
const mongoose = require("mongoose");
const app = express();
const port = 3001

// connecting to mongodbAtlas
mongoose.connect("mongodb+srv://marcokalalo:admin@wdc028-course-booking.vyczd.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
})
// notification for connection success/failure
let db = mongoose.connection;
// if error occurs, output will be...
db.on("error", console.error.bind(console, "connection error"))
// if the connection is successfull...
db.once("open",()=> console.log(`We're connected to the cloud database`))


app.use(express.json());
app.use(express.urlencoded({ extended:true }));


// Mongoose schema/s - the structure of the document that to be created; acts as blueprint to the data
	// schema constructor - to be used in creating a new schema object

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// default values are predefined values for a field if there's no value
		default: "pending"
	}
})

// model - allows access to methods that will perform CRUD operations in the database; capital first letter for the variable of the model and must be in singular form
/*
syntax: 
const <Variable> = mongoose.model(<Variable>, <schemaName>)

*/
const Task = mongoose.model("Task", taskSchema)


// routes
/*
business logic:
1. check if the task is already existing
	if the task exists, return there is a duplicate
	if the task does not exists, it can be added in the database

2. the task data will be coming from the request's body

3. create new task object with the needed properties

4. save to db

*/
app.post("/tasks", (req,res)=>{
	// checking for duplicate tasks
	// findOne is a mongoose method that acts similar to find in MongoDB; it returns the first document that matches the search criteria
	Task.findOne( {name:req.body.name}, (error, result) => {
		// if a document is found and matches the information, sent via client...
		if (result !== null && result.name == req.body.name) {
			// return a message
			return res.send (`Duplicate task found`)
		}else{
			//if no document is found
			//create a new task and save it to db
			let newTask = new Task ({
				name: req.body.name
			})
			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving...
				// saveErr is an object that contains details about the error; errors normally comes as object data type
				if (saveErr) {
					return console.error(saveErr)
				}else{
					// .status - returns a status (number coded i.e. 201 for successful creation)
					return res.status(201).send("New Task Created")
				}
			} )
			
		}
	} )
} )

// business logic for retrieving data *************************************
/*
1. retrieve all the documents using the find() functionality
2. if an error is encountered, print the error (error handling)
3. if there are no errors, send a success status back to the client and return an array of documents

*/

app.get("/tasks", (req, res)=>{
// empty object "{}" means refering to all data present in the database
	Task.find( {}, (err, result)=>{
		// if an error exists
		if (err	) {
		return console.error(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}

	} )

})

//***********ACTIVITY*****************


/*
Register a user(Business Logic)
1. Find if there are duplicate user
	-If user already exists, we return an error
	-if user doesn't exist, we add it in the database
		-if the username and password are both not blank
		-if blank, send response "BOTH username and password must be provided"
	- both condition has been met, create a new object.
	-Save the new object
		-if error, return an error message
		-else, response a status 201 and "New User Registered"
*/
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
const User = mongoose.model("User", userSchema)

app.post("/register", (req,res)=>{
	User.findOne( {
		username:req.body.username
	}, (error, result) => {
				if (result.username == req.body.username) {
					return res.send (`This username already exists`)
				}else {
					if (req.body.username !== "" && req.body.password !== "") {
						let newUser = new User ({
							username: req.body.username,
							password: req.body.password
							})
						newUser.save((saveErr, savedTask) => {
								if (saveErr) {
									return console.error(saveErr)
								}
								else{
									return res.status(201).send (`New user created`)
								}	
							})
					}else{
						return `both username and password must be provided`
					}
				}
			} )
} )

app.get("/users", (req, res)=>{
	User.find( {}, (err, result)=>{
		if (err	) {
		return console.error(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}

	} )

})



app.listen(port, ()=> console.log(`Server is running at port ${port}`));
